
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

public class Server implements ServerInterface{
    
    static String serverName = "server";
    List<Carta> cartas = new ArrayList();
    
    
    public static void main(String args[]) {
        try {
            Server server = new Server();
            ServerInterface stub = (ServerInterface) UnicastRemoteObject.exportObject(server, 0);
            
            Registry registro = LocateRegistry.createRegistry(1099);
            registro.bind(serverName, stub);
            
            System.out.println("Server started");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Override
    public String iniciar(Carta recebido) {
        
        cartas.add(recebido);
        int aux = 0, aux1 = 0, player = 2;
        String ganhador = "Empate!";
        
        try {
            while (aux != 1) {
                
                if (cartas.size() == 2) {
                    int aux2 = compare();

                    if(aux2 == 0) ganhador = "Empate!";
                    else if(player == aux2) ganhador = "Voce Ganhou!";
                    else ganhador = "Voce Perdeu!";

                    aux = 1;
                    aux1++;
                    if(aux1 == 2) cartas.clear();
                } else {
                    player = 1;
                    aux = 0;
                }
                
                synchronized(this) {
                    this.wait(1000);
                }
                
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        
        
        return ganhador;
    }
    
    public int compare() {
        Carta carta1 = cartas.get(0), carta2 = cartas.get(1);
        int pontosP1 = 0, pontosP2 = 0;
        
        if(carta1.getForca() > carta2.getForca()) {
            pontosP1++;
        } else if(carta1.getForca() < carta2.getForca()) {
            pontosP2++;
        }
        
        if(carta1.getInteligencia()> carta2.getInteligencia()) {
            pontosP1++;
        } else if(carta1.getInteligencia() < carta2.getInteligencia()) {
            pontosP2++;
        }
        
        if(carta1.getVelocidade()> carta2.getVelocidade()) {
            pontosP1++;
        } else if(carta1.getVelocidade() < carta2.getVelocidade()) {
            pontosP2++;
        }
        
        if(pontosP1 > pontosP2) {
            return 1;
        } else if(pontosP1 < pontosP2) {
            return 2;
        } else {
            return 0;
        }        
    }
}
