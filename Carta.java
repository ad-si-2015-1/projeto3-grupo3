import java.io.Serializable;

public class Carta implements Serializable {
	int inteligencia;
    int forca;
    int velocidade;
	public Carta(int inteligencia, int forca, int velocidade) {//cria o objeto carta com os valores fornecidos
        this.inteligencia = inteligencia;
        this.forca = forca;
        this.velocidade = velocidade;
	}
	public void show() {//mostra os valores da carta
        System.out.println("Inteliencia: " + inteligencia);
        System.out.println("Força: " + forca);
        System.out.println("Velocidade: " + velocidade);
    }
    public int getInteligencia() {
        return inteligencia;
	}
	public int getForca() {
        return forca;
    }
    public int getVelocidade() {
        return velocidade;
    }
}
