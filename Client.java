
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Random;
import java.util.Scanner;

public class Client {
    
    String serverName = "server";
    
    public static void main(String args[]) {
        Client client = new Client();
        client.connectServer();
    }
 
    private void connectServer() {
        try {
            Registry reg = LocateRegistry.getRegistry("localhost", 1099);
            ServerInterface stub = (ServerInterface) reg.lookup(serverName);
            System.out.println("Connected to Server");
            
            Carta carta = gerarCarta();
            
            System.out.println("Essa é a sua carta:");
            carta.show();
            System.out.println("\n\nEnviando carta...");
            System.out.println('\n' + stub.iniciar(carta));
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    private Carta gerarCarta() {
        int totalDePontos = 20;
        int forca = 0, inteligencia = 0, velocidade = 0;
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Distribua os pontos em atributos para sua carta!\n");
        
        boolean entradaCorreta = false;
        
        while(entradaCorreta == false) {
            System.out.println("Total de pontos: " + totalDePontos);
            System.out.println("Força: ");
            
            try {
                forca = scanner.nextInt();
            } catch (Exception e) {
                System.out.println("O valor deve ser um número!\n");
                continue;
            }
            if(forca <= totalDePontos) {
                entradaCorreta = true;
                totalDePontos -= forca;
            } else {
                System.out.println("O valor deve respeitar o total de pontos!\n");
            }
        }
        
        entradaCorreta = false;
        
        while(entradaCorreta == false) {
            System.out.println("Total de pontos: " + totalDePontos);
            System.out.println("Inteligência: ");
            
            try {
                inteligencia = scanner.nextInt();
            } catch (Exception e) {
                System.out.println("O valor deve ser um número!\n");
                continue;
            }
            if(inteligencia <= totalDePontos) {
                entradaCorreta = true;
                totalDePontos -= inteligencia;
            } else {
                System.out.println("O valor deve respeitar o total de pontos!\n");
            }
        }
        
        entradaCorreta = false;
        
        while(entradaCorreta == false) {
            System.out.println("Total de pontos: " + totalDePontos);
            System.out.println("Velocidade: ");
            
            velocidade = totalDePontos;
            entradaCorreta = true;
        }
        
        System.out.println("\n\n\n\n\n");
        
        Carta carta = new Carta(forca, inteligencia, velocidade);
        return carta;
    }
    
    private Carta gerarCartaAleatoria() {
        int totalDePontos = 20;
        Random gerador = new Random();
        totalDePontos = 20;
        
        int forca = gerador.nextInt(totalDePontos);
        totalDePontos -=forca;
        
        int inteligencia = gerador.nextInt(totalDePontos);
        totalDePontos -=inteligencia;
        
        int velocidade = totalDePontos;
        
        return new Carta(forca, inteligencia, velocidade);
    }
    
}
